import pygame, sys
from pygame.locals import *


def main():
    pygame.init()

    screenSize = (500,500)
    DISPLAY=pygame.display.set_mode( screenSize ,0,32)

    WHITE=(255,255,255)
    BLUE=(50,50,128)
    YELLOW=(255,255,0)

    DISPLAY.fill(WHITE)

    size = 50
    center = (100,250)

    # each of these is a list of x,y,speed
    circles = [[100,101,1], [200,300,1], [300,300,-1], [400,250,1]]

    while True:

        DISPLAY.fill(WHITE)

        for circle in circles:
            x=circle[0]
            y=circle[1]
            speed=circle[2]

            center = (x,y)
            pygame.draw.circle(DISPLAY, BLUE, center, size)
            
            y = y + speed
            if ( y > 400 ):
                speed = -1
            if ( y < 100 ): 
                speed = 1

            circle[0]=x
            circle[1]=y
            circle[2]=speed

        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

main()