import pygame, sys
from pygame.locals import *

def main():
    pygame.init()

    DISPLAY=pygame.display.set_mode((500,500),0,32)

    WHITE=(255,255,255)
    BLUE=(200,200,255)

    DISPLAY.fill(WHITE)

    pygame.draw.circle(DISPLAY,BLUE,(250,250),100)

    while True:
        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

main()