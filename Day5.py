import pygame, sys
from pygame.locals import *

def main():
    pygame.init()

    screenSize = (500,500)
    DISPLAY=pygame.display.set_mode( screenSize ,0,32)

    WHITE=(255,255,255)
    BLUE=(50,50,128)
    YELLOW=(255,255,0)

    DISPLAY.fill(WHITE)

    size = 50
    center = (100,250)

    x=250
    y=0

    while True:

        DISPLAY.fill(WHITE)
        center = (x,y)
        pygame.draw.circle(DISPLAY, BLUE, center, size)
        y = y + 1

        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()

main()