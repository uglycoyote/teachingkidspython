# Teaching the Kids to Code



# Day 1

* (everything done in an interactive python session)
* What is a variable?
* Assigning numbers to variables
* What is an assignment statement? (and how is equal sign different in programming languages than in math?)
* Doing some math with variables
* Assigning a string to a variable
* Accessing characters with the []
  * Question from Gavin: can you use negatives? in python, yes! it accesses the list in reverse.
* String concatenation with +, (hey * works to!)
* Lists: Assigning a list of strings to a variable (list of groceries)
* Accessing the elements with [], and concatenating lists
* errors: can't concatenate a string to a list
* teaser: `[ x+' is delicious' for x in groceries ]` (didn't teach this but demonstrated it)
  * great question from Gavin, what if you want do that only for the foods that start with a certain letter: `[ x+' is delicious' for x in groceries if x.startswith('k') ] `

**points of confusion:**

* What should be quoted and what should not
* What kind of things can go on the left of assignment statements (`3=x`?  `x+3=5`?)
* Gavin thought it was strange that negative-indexing into a string was not evenly balanced with positively indexing. e.g. `s="mike"`, `s[3]` is the last element going up, but `s[-4]` works
* Gavin for some reason typed "and", and really wanted to know what it did in python
* Finley was playing around with string concatenation and `*` on strings and it turned in to an order-of-operations lesson:
  * `"finley" + " is great" * 5`  vs. `("finley" + " is great") * 5`

# Day 2

* In this lesson, start editing a .py file in sublime, and running on command line
* Review: assigning numbers to variables, adding them together
* New:  need to print the result with a print statement; can't just type the variable name like we could in an interactive session.
* Review: assigning some strings to variables, concatenating, print the result
* Review: make a list
  * Gavin went off exploring:
    * left the square brackets off his list, which I was surprised was valid syntax but it worked he just got a tuple
    * what's a tuple?  "it's kind of like a list".  How is it different? "that's an advanced topic".
    * then he made a list with curly braces `{"one", "two", "three"}`, which I am surprised was valid syntax
      * turns out, this is how you define a literal set!
* for loop!  `for x in groceries: print (x + " is delicious")`

# Day 3

* Conditionals

  * make a for loop that something like the teaser yesterday, does a for loop and prints something conditionally
  * good time to talk about `and` and `or` to make more interesting conditions.  `not` as well

* Functions!

* Take some code and make it in to a (parameterless) function

* Paramaterize something and call the function multiple times with different params

* Return values: rather than having a function which prints internally, lets return a string!

  * make the conditional logic into a separate function "IsDelicious"

* Call a function within a function to make a longer more interesting string

* Recursive function?

  

  