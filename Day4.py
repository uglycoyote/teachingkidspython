
# Lets add some numbers!!!
# x = 10
# y = 20
# print ( x )
# print ( y )
# print ( x + y )

# # Now we are going to write my name.
# name = "mike "
# lastname = "cline"
# fullname = name + lastname
# print (fullname)

# Lets make a list
groceries = ["apples", "kool-aid", "bananas", "beans", "toilet paper",
 "ketchup", "food"]

# This function prints the delicious foods
def deliciousGroceries(letter):
	print ("================================")
	for x in groceries:
		if x.startswith(letter):
			print(x + " is delicious because it starts with " + letter)

deliciousGroceries("ke")
deliciousGroceries("b")

print ("\n")