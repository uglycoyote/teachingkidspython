
# Lets add some numbers!!!
# x = 10
# y = 20
# print ( x )
# print ( y )
# print ( x + y )

# # Now we are going to write my name.
# name = "mike "
# lastname = "cline"
# fullname = name + lastname
# print (fullname)

# Lets make a list
groceries = ["apples", "kool-aid", "bananas", "toilet paper", "ketchup", "food"]

def deliciousGroceries():
	print ("DELICIOUS FOODS")
	for x in groceries:
		if x.startswith("k"):
			print(x + " is delicious because it starts with k")
		if x.endswith("d"):
			print(x + " is delicious because it ends with d")

deliciousGroceries()
deliciousGroceries()
deliciousGroceries()