import pygame, sys
from pygame.locals import *

def main():
    pygame.init()

    screenSize = (500,500)
    DISPLAY=pygame.display.set_mode( screenSize ,0,32)

    pineapple = pygame.image.load("pineapple.png")
    imagerect = pineapple.get_rect()

    WHITE=(255,255,255)
    BLUE=(50,50,128)
    YELLOW=(255,255,0)

    DISPLAY.fill(WHITE)

    size = 50
    center = (100,250)

    x=250
    y=250
    speed=1

    while True:

        DISPLAY.fill(WHITE)
        center = (x,y)
        #pygame.draw.circle(DISPLAY, BLUE, center, size)

        DISPLAY.blit(pineapple, center)

        y = y + speed
        if ( y > 400 ): 
            speed = -speed
        if ( y < 100 ): 
            speed = -speed


        for event in pygame.event.get():
            if event.type==QUIT:
                pygame.quit()
                sys.exit()

        pygame.time.wait(2)

        pygame.display.update()

main()